# Java GEDCOM Reader

GEnealogical Data COMmunication (GEDCOM) is a data exchange format for genealogical data. The current [Specification][specification] 5.5.1 from 1999.   

The GEDCOM Reader is a Java library to examine the family and person information from a GEDCOM file.

> **Example**
> ```java
> GedComReader reader = new GedComReader();
> GedCom gedcom = reader.read(new InputStreamReader(getClass().getResourceAsStream("base.ged"), UTF_8));
> Person me = gedcom.findByName("Jens /Kaiser/");
> ```

[specification]: http://phpgedview.sourceforge.net/ged551-5.pdf 