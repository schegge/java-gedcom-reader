package de.schegge.genealogy.gedcom;

import static java.util.Arrays.stream;

public enum Language {
	Afrikaans, Albanian, AngloSaxon("Anglo-Saxon"), Catalan, Catalan_Spn, Czech, Danish, Dutch, English, Esperanto,
	Estonian, Faroese, Finnish, French, German, Hawaiian, Hungarian, Icelandic, Indonesian, Italian, Latvian,
	Lithuanian, Navaho, Norwegian, Polish, Portuguese, Romanian, Serbo_Croa, Slovak, Slovene, Spanish, Swedish, Turkish,
	Wendic,
	// with unicode
	Amharic, Arabic, Armenian, Assamese, Belorusian, Bengali, Braj, Bulgarian, Burmese, Cantonese,
	ChurchSlavic("Church-Slavic"), Dogri, Georgian, Greek, Gujarati, Hebrew, Hindi, Japanese, Kannada, Khmer, Konkani,
	Korean, Lahnda, Lao, Macedonian, Maithili, Malayalam, Mandrin, Manipuri, Marathi, Mewari, Nepali, Oriya, Pahari,
	Pali, Panjabi, Persian, Prakrit, Pusto, Rajasthani, Russian, Sanskrit, Serb, Tagalog, Tamil, Telugu, Thai, Tibetan,
	Ukrainian, Urdu, Vietnamese, Yiddish;

	private final String value;

	Language() {
		value = name();
	}

	Language(String value) {
		this.value = value;
	}

	public static Language byName(String name) {
		return stream(values()).filter(l -> l.value.equals(name)).findFirst().orElse(null);
	}
}
