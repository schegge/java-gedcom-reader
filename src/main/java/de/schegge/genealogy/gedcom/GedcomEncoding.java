package de.schegge.genealogy.gedcom;

public enum GedcomEncoding {
	UTF_8, UNICODE, ANSI, ASCII, ANSEL;
	public static GedcomEncoding byName(String name) {
		if ("UTF-8".equals(name)) {
			return UTF_8;
		}
		return valueOf(name);
	}
}
