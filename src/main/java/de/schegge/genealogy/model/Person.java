package de.schegge.genealogy.model;

import java.net.URI;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Person {
	private Sex sex;
	private String name;
	private final List<Event> birth = new ArrayList<>(1);
	private final List<Event> death = new ArrayList<>();
	private Family family;
	private String description;
	private String submitterId;
	private String id;
	private URI urn;

	private String firstname;
	private String lastname;
	private boolean alive = true;

	private Set<String> spouse = new HashSet<>();
	private Set<String> child = new HashSet<>();

	public Set<String> getSpouse() {
		return spouse;
	}

	public void setSpouse(Set<String> spouse) {
		this.spouse = spouse;
	}

	public Set<String> getChild() {
		return child;
	}

	public void setChild(Set<String> child) {
		this.child = child;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBirth(Event event) {
		birth.add(event);
	}

	public void addBirth(Event event) {
		birth.add(event);

	}

	public void setDeath(Event event) {
		this.death.add(event);
	}

	public void addDeath(Event event) {
		this.death.add(event);
	}

	public Sex getSex() {
		return sex;
	}

	public String getName() {
		return name;
	}

	public Event getBirth() {
		return birth.stream().findFirst().orElse(new Event());
	}

	public Event getDeath() {
		return death.stream().findFirst().orElse(new Event());
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public String getSubmitterId() {
		return submitterId;
	}

	public void setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "name=" + name + ", id=" + id + ", urn=" + urn;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Person) {
			Person p = (Person) o;
			return Objects.equals(id, p.id) && Objects.equals(name, p.name) && Objects.equals(sex, p.sex)
					&& Objects.equals(birth, p.birth);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, sex, birth);
	}

	public int getAge(Event event) {
		final LocalDate date = to(event.getDate());
		return date == null ? -1 : getAge(date);
	}

	public int getAge(LocalDate date) {
		final LocalDate birthDate = to(birth.stream().map(Event::getDate).filter(Objects::nonNull).findFirst().orElse(null));
		if (birthDate == null || date == null) {
			return -1;
		}
		return Period.between(birthDate, date).getYears();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUrn(URI urn) {
		this.urn = urn;
	}

	public URI getUrn() {
		return urn;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	private static LocalDate to(Temporal date) {
		if (date == null || !date.isSupported(ChronoField.DAY_OF_MONTH)) {
			return null;
		}
		return LocalDate.from(date);
	}

}
