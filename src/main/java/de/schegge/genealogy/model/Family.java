package de.schegge.genealogy.model;

import java.util.ArrayList;
import java.util.List;

public class Family {
	private String id;
	private Person husband;
	private Person wife;
	private final List<Person> children = new ArrayList<>();

	private Event marriage;

	public Family() {
		super();
	}

	public Family(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Person getHusband() {
		return husband;
	}

	public void setHusband(Person husband) {
		this.husband = husband;
	}

	public Person getWife() {
		return wife;
	}

	public void setWife(Person wife) {
		this.wife = wife;
	}

	public List<Person> getChildren() {
		return children;
	}

	public Event getMarriage() {
		return marriage;
	}

	public void setMarriage(Event marriage) {
		this.marriage = marriage;
	}
}
