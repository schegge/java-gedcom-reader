package de.schegge.genealogy.model;

import static java.text.MessageFormat.format;

public class Source {
	private String id;
	private String version;
	private String name;
	private String corporation;

	public Source() {
		super();
	}

	public Source(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCorporation() {
		return corporation;
	}

	public void setCorporation(String corporation) {
		this.corporation = corporation;
	}

	@Override
	public String toString() {
		return format("Source: {0} ({1}) {2}", name, version, corporation);
	}
}
