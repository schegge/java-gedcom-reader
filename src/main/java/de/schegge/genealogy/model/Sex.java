package de.schegge.genealogy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Sex {
	@JsonProperty("-")
	UNKNOWN, //
	@JsonProperty("♂")
	MALE, //
	@JsonProperty("♀")
	FEMALE;

	public static Sex byToken(String token) {
		if ("M".equals(token)) {
			return MALE;
		}
		if ("F".equals(token)) {
			return FEMALE;
		}
		return UNKNOWN;
	}
}