/*
 * Info.java
 *
 * Author: KAIS114
 * Copyright (c) 2019 arvato
 */
package de.schegge.genealogy.model;

import de.schegge.genealogy.gedcom.Language;

public class Info {
  private Language language;
  private String note;

  public Info() {
    super();
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
