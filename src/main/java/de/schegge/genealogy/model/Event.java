package de.schegge.genealogy.model;

import java.time.temporal.Temporal;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class Event {
	private Temporal date;
	private String place;

	public void setDate(Temporal date) {
		this.date = date;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Temporal getDate() {
		return date;
	}

	public String getPlace() {
		return place;
	}

	@JsonIgnore
	public Optional<Temporal> getOptionalDate() {
		return Optional.ofNullable(date);
	}

	@JsonIgnore
	public Optional<String> getOptionalPlace() {
		return Optional.ofNullable(place);
	}
}
