package de.schegge.genealogy;

import java.util.Optional;

import de.schegge.genealogy.gedcom.Type;

class Token {
	private int level;
	private Type type;
	private String content;
	private String reference;
	private String line;

	public Token() {
		this.level = -1;
	}

	public Token(int level, String reference, Type type,
			String content, String line) {
		this.level = level;
		this.reference = reference;
		this.type = type;
		this.content = content;
		this.line = line;
	}

	public int getLevel() {
		return level;
	}

	public Type getType() {
		return type;
	}

	public String getContent() {
		return content;
	}

	public Optional<String> getReference() {
		return Optional.ofNullable(reference);
	}

	public Optional<String> getReferenceValue() {
		if (content.startsWith("@") && content.endsWith("@")) {
			return Optional.of(content.substring(1, content.length() - 1));
		}
		return Optional.empty();
	}

	public String getLine() {
		return line;
	}

	public void setContent(String content) {
		this.content = content;
	}
}