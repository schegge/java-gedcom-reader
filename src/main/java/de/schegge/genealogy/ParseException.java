package de.schegge.genealogy;

import java.io.IOException;

public class ParseException extends IOException {
	public ParseException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
