package de.schegge.genealogy;

import de.schegge.genealogy.model.Info;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import de.schegge.genealogy.model.Family;
import de.schegge.genealogy.model.Person;
import de.schegge.genealogy.model.Source;

public class GedCom {
	private static final Logger LOGGER = Logger.getLogger(GedCom.class.getName());

	private Source source;
	private Info info;
	private List<Person> individuals;
	private List<Family> families;

	public List<Person> getIndividuals() {
		return individuals;
	}

	public void setIndividuals(List<Person> individuals) {
		LOGGER.info(individuals.toString());
		this.individuals = individuals;
	}

	public List<Family> getFamilies() {
		return families;
	}

	public void setFamilies(List<Family> families) {
		this.families = families;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

  public void setInfo(Info info) {
    this.info = info;
  }

  public Info getInfo() {
    return info;
  }

  public Optional<Person> findPerson(String name) {
		return individuals.stream().filter(p -> p.getName().equals(name)).findFirst();
	}

	public Optional<Person> findByName(String name) {
		return findPerson(name);
	}

	public Optional<Person> findById(String id) {
		return individuals.stream().filter(p -> p.getId().equals(id)).findFirst();
	}

	public Optional<Person> findByUrn(String urnPart) {
		URI urn = URI.create("urn:gina:" + urnPart);
		LOGGER.info(urnPart + " " + urn);
		return individuals.stream().filter(p -> p.getUrn().equals(urn)).findFirst();
	}

	public Optional<Family> findFamily(String id) {
		return families.stream().filter(p -> p.getId().equals(id)).findFirst();
	}
}
