package de.schegge.genealogy;

import static de.schegge.genealogy.gedcom.Type.UNSUPPORTED;
import static java.lang.Integer.parseInt;
import static java.text.MessageFormat.format;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.schegge.genealogy.gedcom.Type;

public class TokenReader implements Closeable {

  private static final Pattern PATTERN = Pattern.compile("\\s*(\\d+) (@([-A-Za-z0-9_]+)@ )?([A-Z0-9_]+)( (.*))?$");

  private final static Token NULL_TOKEN = new Token();
  private final static Token BOF_TOKEN = new Token();
  private final static Token EOF_TOKEN = new Token();

  private final static Token CONT_TOKEN = new Token();

  private BufferedReader reader;
  private Token nextToken;
  private Token currentToken = BOF_TOKEN;
  private Token pushBackToken = NULL_TOKEN;
  private int line;
  private boolean ignoreLineLength;

  public int getCurrentLine() {
    return line;
  }

  public void pushBack() {
    pushBackToken = currentToken;
  }

  private String readLine() throws IOException {
    line++;
    return checkLength(reader.readLine());
  }

  private String checkLength(String line) throws ParseException {
    if (line != null && line.length() > 255 && !ignoreLineLength) {
      throw new ParseException("line exceeds limit of 255: " + line.length() + " " + this.line);
    }
    return line;
  }

  public TokenReader(Reader input) {
    this(input, false);
  }

  public TokenReader(Reader input, boolean ignoreLineLength) {
    this.ignoreLineLength = ignoreLineLength;
    reader = new BufferedReader(input);
    try {
      nextToken = parseToken(readLine());
    } catch (IOException e) {
      nextToken = EOF_TOKEN;
    }
  }

  public boolean hasNext() {
    return pushBackToken != NULL_TOKEN || nextToken.getLevel() != -1;
  }

  public void next() throws IOException {
    if (pushBackToken.getLevel() != -1) {
      currentToken = pushBackToken;
      pushBackToken = NULL_TOKEN;
      return;
    }
    if (nextToken == EOF_TOKEN) {
      throw new ParseException("EOF");
    }
    if (nextToken.getLevel() == -1) {
      throw new ParseException("cannot parse: " + nextToken.getLine());
    }
    currentToken = nextToken;
    do {
      nextToken = parseToken(readLine());
      if (nextToken == EOF_TOKEN) {
        return;
      }

      if (Type.CONC == nextToken.getType()) {
        handleContinueToken("");
      } else if (Type.CONT == nextToken.getType()) {
        handleContinueToken("\n");
      }
    } while (nextToken == CONT_TOKEN);
  }

  private void handleContinueToken(String delimiter) {
    if (currentToken.getLevel() + 1 != nextToken.getLevel()) {
      throw new IllegalArgumentException("wrong continuation level: " + nextToken.getLevel());
    }
    currentToken.setContent(currentToken.getContent() + delimiter + nextToken.getContent());
    nextToken = CONT_TOKEN;
  }

  private static Token parseToken(String result) {
    if (result == null) {
      return EOF_TOKEN;
    }
    Matcher matcher = PATTERN.matcher(result);
    if (!matcher.matches()) {
      return new Token(-1, null, UNSUPPORTED, null, result);
    }
    final int level = parseInt(matcher.group(1));
    try {
      return new Token(level, matcher.group(3), Type.valueOf(matcher.group(4)), matcher.group(6), result);
    } catch (IllegalArgumentException e) {
      return new Token(level, matcher.group(3), UNSUPPORTED, matcher.group(6), result);
    }
  }

  public void check(int level) throws ParseException {
    if (getLevel() != level) {
      throw new ParseException(format("illegal level: {0}", getLine()));
    }
  }

  public void check(Type type, int level) throws ParseException {
    check(level);
    if (getType() != type) {
      throw new ParseException(format("illegal type: {0}", getType()));
    }
  }

  public int getLevel() {
    return currentToken.getLevel();
  }

  public String getLine() {
    return currentToken.getLine();
  }

  public Type getType() {
    return currentToken.getType();
  }

  public String getContent() {
    return currentToken.getContent();
  }

  public String getReferenceValue() {
    return currentToken.getReferenceValue().orElse(null);
  }

  @Override
  public void close() throws IOException {
    if (reader != null) {
      reader.close();
    }
  }

  public String getReference() {
    return currentToken.getReference().orElse(null);
  }
}
