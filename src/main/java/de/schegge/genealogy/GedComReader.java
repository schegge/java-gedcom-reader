package de.schegge.genealogy;

import static java.lang.String.format;
import static java.util.Arrays.asList;

import de.schegge.genealogy.model.Info;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.schegge.genealogy.gedcom.GedcomEncoding;
import de.schegge.genealogy.gedcom.Language;
import de.schegge.genealogy.gedcom.Type;
import de.schegge.genealogy.model.Event;
import de.schegge.genealogy.model.Family;
import de.schegge.genealogy.model.Person;
import de.schegge.genealogy.model.Sex;
import de.schegge.genealogy.model.Source;

public class GedComReader {
	private static final Logger LOGGER = LoggerFactory.getLogger(GedComReader.class);

	private static final List<String> SUPPORTED_VERSION = asList("5.5.1", "5.5");

	private static final DateTimeFormatter DATE_FORMAT = new DateTimeFormatterBuilder().parseCaseInsensitive()
			.appendPattern("[[dd ]MMM ]yyyy").toFormatter(Locale.ENGLISH);

  public GedCom read(Reader input) throws IOException {
    return read(input, false);
  }

  public GedCom read(Reader input, boolean ignoreLineLength) throws IOException {
    Map<String, Family> families = new HashMap<>();
    Map<String, Person> individuals = new HashMap<>();
		GedCom result = new GedCom();
		try (TokenReader reader = new TokenReader(input, ignoreLineLength)) {
			readHeader(reader, result);
			String currentSubmitter = null;
			while (reader.hasNext()) {
				if (reader.getType() == Type.TRLR) {
					reader.pushBack();
					break;
				}
				switch (reader.getType()) {
				case SUBM:
					currentSubmitter = reader.getReference();
					readUntil(reader, 0);
					break;
				case INDI:
					readIndividuals(reader, result, currentSubmitter, individuals);
					currentSubmitter = null;
					break;
				case FAM:
					readFamilies(reader, result, families, individuals);
					break;
				default:
					readUntil(reader, 0);
				}
			}
			readTrailer(reader);
		}
		result.setFamilies(new ArrayList<>(families.values()));
		return result;
	}

	private void readFamilies(TokenReader reader, GedCom result, Map<String, Family> families, Map<String, Person> individuals) throws IOException {
		reader.pushBack();
		while (reader.hasNext()) {
			reader.next();
			if (reader.getType() != Type.FAM) {
				reader.pushBack();
				return;
			}

			reader.check(Type.FAM, 0);
			Family family = families.computeIfAbsent(reader.getReference(), Family::new);
			LOGGER.info("family: {}", family.getId());
			while (reader.hasNext()) {
				reader.next();
				if (reader.getLevel() < 1) {
					reader.pushBack();
					break;
				}
				switch (reader.getType()) {
				case HUSB:
					Person husband = individuals.get(reader.getReferenceValue());
					LOGGER.debug("husband: {}", husband);
					if (!husband.getSpouse().contains(family.getId())) {
						throw new IllegalArgumentException(
								format("invalid data on line %d, person %s not defined as husband in family %s",
										reader.getCurrentLine(), husband.getName(), family.getId()));
					}
					family.setHusband(husband);
					break;
				case WIFE:
					Person wife = individuals.get(reader.getReferenceValue());
					LOGGER.debug("wife: {}", wife);
					if (!wife.getSpouse().contains(family.getId())) {
						throw new IllegalArgumentException(format("invalid data, person not defined as wife in family %s, %d", wife, reader.getCurrentLine()));
					}
					family.setWife(wife);
					break;
				case CHIL:
					Person child = individuals.get(reader.getReferenceValue());
					LOGGER.debug("child: {}", child);
					if (!child.getChild().contains(family.getId())) {
						throw new IllegalArgumentException(format("invalid data, person not defined as child in family %s %d",
								child.getChild(), reader.getCurrentLine()));
					}
          family.getChildren().add(child);
					child.setFamily(family);
					break;
				case MARR:
					family.setMarriage(readEvent(reader));
					break;
				default:
					readUntil(reader, 1);
				}
			}
		}
		LOGGER.debug("families: {} {}", families.size(), families);
		result.setFamilies(new ArrayList<>(families.values()));
	}

	private void readIndividuals(TokenReader reader, GedCom result, String submitter, Map<String, Person> individuals) throws IOException {
		List<Person> persons = new ArrayList<>();
		reader.pushBack();
		while (reader.hasNext()) {
			reader.next();
			if (reader.getType() != Type.INDI) {
				reader.pushBack();
				break;
			}
			reader.check(Type.INDI, 0);
			Person person = new Person();
			person.setId(reader.getReference());
			person.setSubmitterId(submitter);
			person.setSex(Sex.UNKNOWN);
			persons.add(person);
			individuals.put(person.getId(), person);
			while (reader.hasNext()) {
				reader.next();
				if (reader.getLevel() < 1) {
					reader.pushBack();
					break;
				}
				switch (reader.getType()) {
				case DSCR:
					person.setDescription(reader.getContent());
					break;
				case NAME:
					person.setName(reader.getContent());
					break;
				case SEX:
					person.setSex(Sex.byToken(reader.getContent()));
					break;
				case BIRT:
					person.addBirth(readEvent(reader));
					break;
				case DEAT:
					person.setAlive(true);
					if (!"Y".equals(reader.getContent())) {
						person.addDeath(readEvent(reader));
					}
					break;
				case NOTE:
					person.setUrn(readNote(reader));
					break;
				case BAPM:
					readEvent(reader);
					break;
				case FAMC:
          person.getChild().add(reader.getReferenceValue());
					break;
				case FAMS:
          person.getSpouse().add(reader.getReferenceValue());
					break;
				case OCCU:
					readEvent(reader);
					break;
				default:
					readUntil(reader, 1);
				}
			}
			if (person.getUrn() == null) {
				person.setUrn(URI.create("urn:gina:schegge.de/" + person.getId()));
			}
		}
		result.setIndividuals(persons);
	}

	private URI readNote(TokenReader reader) {
		String content = reader.getContent();
		if (content.startsWith("http://")) {
			Matcher matcher = Pattern.compile("http://gedbas.genealogy.net/person/show/(\\d+)").matcher(content);
			if (matcher.matches()) {
				return URI.create("urn:gina:gebdas/" + matcher.group(1));
			}
		}
		return null;
	}

	private static Event readEvent(TokenReader reader) throws IOException {
		int currentLevel = reader.getLevel();
		Event event = new Event();
		while (reader.hasNext()) {
			reader.next();
			if (reader.getLevel() <= currentLevel) {
				reader.pushBack();
				break;
			}
			switch (reader.getType()) {
			case DATE:
				event.setDate(readDateOnLevel(reader, currentLevel + 1));
				break;
			case PLAC:
				event.setPlace(readPlaceOnLevel(reader, currentLevel + 1));
				break;
			case CAUS:
				break;
			case SOUR:
				break;
			default:
				readUntil(reader, currentLevel + 1);
			}
		}
		return event;
	}

	private static Temporal readDateOnLevel(TokenReader reader, int level) throws IOException {
		String date = reader.getContent();
		reader.next();
		readTimeOnLevel(reader, level + 1);
		return parseDate(date);
	}

	private static void readTimeOnLevel(TokenReader reader, int level) {
		if (Type.TIME != reader.getType() || reader.getLevel() != level) {
			reader.pushBack();
		}
	}

	private static Temporal parseDate(String temporal) {
		try {
			TemporalAccessor accessor = DATE_FORMAT.parse(temporal);
			if (accessor.isSupported(ChronoField.DAY_OF_MONTH)) {
				return LocalDate.from(accessor);
			}
			if (accessor.isSupported(ChronoField.MONTH_OF_YEAR)) {
				return YearMonth.from(accessor);
			}
			return Year.from(accessor);
		} catch (DateTimeException e) {
			return null;
		}
	}

	private static String readPlaceOnLevel(TokenReader reader, int level) {
		return reader.getContent();
	}

	private static void readHeader(TokenReader reader, GedCom result) throws IOException {
		reader.next();
		reader.check(Type.HEAD, 0);
		result.setInfo(new Info());
		while (reader.hasNext()) {
			reader.next();
			if (reader.getLevel() < 1) {
				reader.pushBack();
				break;
			}
			switch (reader.getType()) {
			case SOUR:
				readSource(reader, result);
				break;
			case GEDC:
				readGedc(reader);
				break;
			case CHAR:
				readEncoding(reader);
				break;
			case LANG:
				result.getInfo().setLanguage(Language.byName(reader.getContent()));
				break;
			case NOTE:
				result.getInfo().setNote(reader.getContent());
				break;
			case DATE:
				break;
			default:
				readUntil(reader, 1);
			}
		}
	}

	private static void readEncoding(TokenReader reader) throws ParseException {
		String content = reader.getContent();
		try {
			GedcomEncoding encoding = GedcomEncoding.byName(content);
			LOGGER.info("encoding: " + encoding);
		} catch (RuntimeException e) {
			throw new ParseException("Illegal encoding: " + content);
		}
	}

	private static void readGedc(TokenReader reader) throws IOException {
		while (reader.hasNext()) {
			reader.next();
			if (reader.getLevel() < 2) {
				reader.pushBack();
				return;
			}
      if (reader.getType() == Type.VERS) {
        String version = reader.getContent();
        if (!SUPPORTED_VERSION.contains(version)) {
          throw new ParseException("Unsupported version: " + version);
        }
      } else {
        readUntil(reader, 2);
      }
		}
	}

	private static void readTrailer(TokenReader reader) throws IOException {
		reader.next();
		reader.check(Type.TRLR, 0);
		if (reader.hasNext()) {
			throw new ParseException("no records allowed after TRLR");
		}
	}

	private static void readUntil(TokenReader reader, int level) throws IOException {
		int lastLine = reader.getCurrentLine();
		while (reader.hasNext()) {
			reader.next();
			if (reader.getLevel() <= level && lastLine != reader.getCurrentLine()) {
				reader.pushBack();
				return;
			}
		}
	}

	private static void readSource(TokenReader reader, GedCom result) throws IOException {
		Source source = new Source(reader.getContent());
		while (reader.hasNext()) {
			reader.next();
			if (reader.getLevel() < 2) {
				reader.pushBack();
				break;
			}
			switch (reader.getType()) {
			case VERS:
				source.setVersion(reader.getContent());
				break;
			case NAME:
				source.setName(reader.getContent());
				break;
			case CORP:
        readCorporate(reader, source);
        break;
			default:
				readUntil(reader, 2);
			}
		}
		result.setSource(source);
	}

  private static void readCorporate(TokenReader reader, Source source) throws IOException {
    source.setCorporation(reader.getContent());
    while (reader.hasNext()) {
      reader.next();
      if (reader.getLevel() < 3) {
        reader.pushBack();
        break;
      }
    }
  }
}
