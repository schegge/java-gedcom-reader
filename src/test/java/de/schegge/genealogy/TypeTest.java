package de.schegge.genealogy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.schegge.genealogy.gedcom.Type;

class TypeTest {
	@Test
	void test() {
		assertEquals(131, Type.WILL.ordinal());
	}
}
