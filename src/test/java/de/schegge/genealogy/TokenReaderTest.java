package de.schegge.genealogy;

import static de.schegge.genealogy.gedcom.Type.HEAD;
import static de.schegge.genealogy.gedcom.Type.VERS;
import static de.schegge.genealogy.test.TokenReaderMatchers.hasContent;
import static de.schegge.genealogy.test.TokenReaderMatchers.hasLevel;
import static de.schegge.genealogy.test.TokenReaderMatchers.hasNext;
import static de.schegge.genealogy.test.TokenReaderMatchers.hasReference;
import static de.schegge.genealogy.test.TokenReaderMatchers.hasType;
import static java.util.stream.Collectors.joining;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TokenReaderTest {

  @Test
  public void testRead() throws IOException {
    Reader input = new StringReader("0 HEAD\n" + "0 VERS content\n");
    try (TokenReader reader = new TokenReader(input)) {
      assertThat(reader, hasNext());
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(HEAD), hasNext()));
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(VERS), not(hasNext()), hasContent("content")));
    }
  }

  @Test
  public void testReadCont() throws IOException {
    Reader input = new StringReader("0 HEAD\n" + "0 VERS content\n" + "1 CONC  content");
    try (TokenReader reader = new TokenReader(input)) {
      assertThat(reader, hasNext());
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(HEAD), hasNext()));
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(VERS), not(hasNext()), hasContent("content content")));
    }
  }

  @Test
  public void testReadReference() throws IOException {
    Reader input = new StringReader("0 HEAD\n" + "0 @REF@ VERS\n" + "0 THIRD content");
    try (TokenReader reader = new TokenReader(input)) {
      assertThat(reader, hasNext());
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(HEAD), hasNext()));
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(VERS), hasNext(), hasReference("REF")));
    }
  }

  @Test
  public void testIgnoreLeadingWhitespace() throws IOException {
    Reader input = new StringReader("  0 HEAD\n" + "   0 @REF@ VERS\n" + " 0 THIRD content");

    try (TokenReader reader = new TokenReader(input)) {
      assertThat(reader, hasNext());
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(HEAD), hasNext()));
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(VERS), hasNext(), hasReference("REF")));
    }
  }

  @Test
  public void testErrorOnLongLines() throws IOException {
    Reader input = new StringReader("0 HEAD\n" + "0 @REF@ VERS\n" + "0 THIRD "
        + IntStream.range(0, 256).mapToObj(x -> "x").collect(joining()));
    try (TokenReader reader = new TokenReader(input)) {
      assertThat(reader, hasNext());
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(HEAD), hasNext()));
      IOException e = Assertions.assertThrows(IOException.class, reader::next);
      Assertions.assertEquals("line exceeds limit of 255: 264 3", e.getMessage());
    }
  }

  @Test
  public void ignoreErrorOnLongLines() throws IOException {
    Reader input = new StringReader("0 HEAD\n" + "0 @REF@ VERS\n" + "0 THIRD "
        + IntStream.range(0, 256).mapToObj(x -> "x").collect(joining()));
    try (TokenReader reader = new TokenReader(input, true)) {
      assertThat(reader, hasNext());
      reader.next();
      assertThat(reader, allOf(hasLevel(0), hasType(HEAD), hasNext()));
      reader.next();
    }
  }
}
