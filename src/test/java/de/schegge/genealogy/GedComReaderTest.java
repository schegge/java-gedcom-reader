package de.schegge.genealogy;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.schegge.genealogy.gedcom.Language;

public class GedComReaderTest {

  private GedComReader reader;

  @BeforeEach
  public void setUp() {
    reader = new GedComReader();
  }

  @Test
  public void testReadHeader() throws IOException {
    Reader input = new StringReader(
// @formatter:off
        "0 HEAD\n" + "1 SOUR FTM\n" + "2 VERS Family Tree Maker (21.1.0.865)\n"
            + "2 NAME Family Tree Maker for Windows\n" + "2 CORP Ancestry.com\n" + "3 ADDR 360 W 4800 N\n"
            + "4 CONT Provo, UT 84604\n" + "3 PHON (801) 705-7000\n" + "1 DEST GED55\n"
            + "1 DATE 30 MAR 2015\n" + "1 CHAR UTF-8\n" + "1 LANG German\n"
            + "1 FILE C:\\Users\\kai13\\Desktop\\Kaiser-Familienstammbaum_2015-03-30.ged\n"
            + "1 SUBM @SUBM@\n" + "1 GEDC\n" + "2 VERS 5.5.1\n" + "2 FORM LINEAGE-LINKED\n" + "0 TRLR\n");
// @formatter:on
    GedCom object = reader.read(input);
    assertThat(object, is(notNullValue()));
    assertThat(object.getSource().getId(), is("FTM"));
    assertThat(object.getSource().getVersion(), is("Family Tree Maker (21.1.0.865)"));
    assertThat(object.getSource().getName(), is("Family Tree Maker for Windows"));
    assertThat(object.getInfo().getLanguage(), is(Language.German));
    assertThat(object.getSource().getCorporation(), is("Ancestry.com"));
  }

  @Test
  public void testReadHeaderAnselEncoding() throws IOException {
    Reader input = new StringReader(
// @formatter:off
        "0 HEAD\n" + "1 SOUR FTM\n" + "2 VERS Family Tree Maker (21.1.0.865)\n"
            + "2 NAME Family Tree Maker for Windows\n" + "2 CORP Ancestry.com\n" + "3 ADDR 360 W 4800 N\n"
            + "4 CONT Provo, UT 84604\n" + "3 PHON (801) 705-7000\n" + "1 DEST GED55\n"
            + "1 DATE 30 MAR 2015\n" + "1 CHAR ANSEL\n" + "1 LANG German\n"
            + "1 FILE C:\\Users\\kai13\\Desktop\\Kaiser-Familienstammbaum_2015-03-30.ged\n"
            + "1 SUBM @SUBM@\n" + "1 GEDC\n" + "2 VERS 5.5\n" + "2 FORM LINEAGE-LINKED\n" + "0 TRLR\n");
// @formatter:on
    GedCom object = reader.read(input);
    assertThat(object, is(notNullValue()));
    assertThat(object.getSource().getId(), is("FTM"));
    assertThat(object.getSource().getVersion(), is("Family Tree Maker (21.1.0.865)"));
    assertThat(object.getSource().getName(), is("Family Tree Maker for Windows"));
    assertThat(object.getInfo().getLanguage(), is(Language.German));
    assertThat(object.getSource().getCorporation(), is("Ancestry.com"));
  }

  @Test
  public void testReadHeaderAndIndi() throws IOException {
    Reader input = new StringReader(
// @formatter:off
        "0 HEAD\n" + "1 SOUR FTM\n" + "2 VERS Family Tree Maker (21.1.0.865)\n"
            + "2 NAME Family Tree Maker for Windows\n" + "2 CORP Ancestry.com\n" + "3 ADDR 360 W 4800 N\n"
            + "4 CONT Provo, UT 84604\n" + "3 PHON (801) 705-7000\n" + "1 DEST GED55\n"
            + "1 DATE 30 MAR 2015\n" + "1 CHAR UTF-8\n"
            + "1 FILE C:\\Users\\kai13\\Desktop\\Kaiser-Familienstammbaum_2015-03-30.ged\n"
            + "1 SUBM @SUBM@\n" + "1 GEDC\n" + "2 VERS 5.5\n" + "2 FORM LINEAGE-LINKED\n" + "0 @P3@ INDI\n"
            + "1 NAME Jens Hans Herrmann /Kaiser/\n" + "1 BIRT\n" + "2 DATE 24 Aug 1968\n"
            + "2 PLAC Bünde, Herford, Nordrhein-Westfalen, Deutschland" + "1 SEX M\n" + "1 FAMC @F2@\n"
            + "1 FAMS @F1@\n"
            + "0 TRLR\n");
// @formatter:on
    GedCom object = reader.read(input);
    assertThat(object.getSource().getId(), is("FTM"));
    assertThat(object.getSource().getVersion(), is("Family Tree Maker (21.1.0.865)"));
    assertThat(object.getSource().getName(), is("Family Tree Maker for Windows"));
    assertThat(object.getSource().getCorporation(), is("Ancestry.com"));
  }

  @Test
  public void testReadEmptyInput() {
    IOException e = assertThrows(IOException.class, () -> reader.read(new StringReader("")));
    assertEquals("EOF", e.getMessage());
  }

  @Test
  public void testReadIllegalLevel() {
    IOException e = assertThrows(IOException.class, () -> reader.read(new StringReader("n HEAD\n")));
    assertEquals("cannot parse: n HEAD", e.getMessage());
  }

  @Test
  public void testReadIllegalHeaderLevel() {
    IOException e = assertThrows(IOException.class, () -> reader.read(new StringReader("1 HEAD\n")));
    assertEquals("illegal level: 1 HEAD", e.getMessage());
  }

  @Test
  public void testReadIllegalGedComVersion() {
    Reader input = new StringReader("0 HEAD\n1 GEDC\n2 VERS 5.5.2\n");
    IOException e = assertThrows(IOException.class, () -> reader.read(input));
    assertEquals("Unsupported version: 5.5.2", e.getMessage());
  }

  @Test
  public void testReadIllegalEncoding() {
    Reader input = new StringReader("0 HEAD\n1 CHAR UTF-7\n");
    IOException e = assertThrows(IOException.class, () -> reader.read(input));
    assertEquals("Illegal encoding: UTF-7", e.getMessage());
  }
}
