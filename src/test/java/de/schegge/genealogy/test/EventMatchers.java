package de.schegge.genealogy.test;

import java.time.temporal.Temporal;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import de.schegge.genealogy.model.Event;

public class EventMatchers {
	public static <T extends Temporal & Comparable<T>> Matcher<Event> date(
			Matcher<T> matcher) {
		return new FeatureMatcher<Event, T>(matcher, "date", "date") {
			@Override
			protected T featureValueOf(Event actual) {
				return (T) actual.getDate();
			}
		};
	}

	public static Matcher<Event> place(Matcher<String> matcher) {
		return new FeatureMatcher<Event, String>(matcher, "place", "place") {
			@Override
			protected String featureValueOf(Event actual) {
				return actual.getPlace();
			}
		};
	}
}
