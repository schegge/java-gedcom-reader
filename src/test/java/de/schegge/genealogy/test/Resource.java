package de.schegge.genealogy.test;/*
 * Resource.java
 *
 * Author: KAIS114
 * Copyright (c) 2019 arvato
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Resource {
String value();
}
