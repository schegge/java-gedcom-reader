package de.schegge.genealogy.test;

import static org.hamcrest.CoreMatchers.equalTo;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import de.schegge.genealogy.TokenReader;
import de.schegge.genealogy.gedcom.Type;

public class TokenReaderMatchers {
	public static Matcher<TokenReader> hasLevel(final int i) {
		return new FeatureMatcher<TokenReader, Integer>(equalTo(i), "level",
				"level") {
			@Override
			protected Integer featureValueOf(final TokenReader actual) {
				return actual.getLevel();
			}
		};
	}

	public static Matcher<TokenReader> hasType(final Type i) {
		return new FeatureMatcher<TokenReader, Type>(equalTo(i), "type",
				"type") {
			@Override
			protected Type featureValueOf(final TokenReader actual) {
				return actual.getType();
			}
		};
	}

	public static Matcher<TokenReader> hasNext() {
		return new FeatureMatcher<TokenReader, Boolean>(equalTo(true), "next",
				"next") {
			@Override
			protected Boolean featureValueOf(final TokenReader actual) {
				return actual.hasNext();
			}
		};
	}

	public static Matcher<TokenReader> hasContent(String content) {
		return new FeatureMatcher<TokenReader, String>(equalTo(content),
				"content", "content") {
			@Override
			protected String featureValueOf(final TokenReader actual) {
				return actual.getContent();
			}
		};
	}

	public static Matcher<TokenReader> hasReference(String reference) {
		return new FeatureMatcher<TokenReader, String>(equalTo(reference),
				"reference", "reference") {
			@Override
			protected String featureValueOf(final TokenReader actual) {
				return actual.getReference();
			}
		};
	}
}
