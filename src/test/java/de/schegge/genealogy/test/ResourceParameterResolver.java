/*
 * ResourceParameterResolver.java
 *
 * Author: KAIS114
 * Copyright (c) 2019 arvato
 */
package de.schegge.genealogy.test;

import de.schegge.genealogy.GedComReader;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Parameter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class ResourceParameterResolver implements ParameterResolver {

  private static final List<Class<?>> PARAMETERS = Arrays.asList(Reader.class, InputStream.class);

  @Override
  public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
      throws ParameterResolutionException {
    return parameterContext.isAnnotated(Resource.class) && PARAMETERS
        .contains(parameterContext.getParameter().getType());
  }

  @Override
  public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
      throws ParameterResolutionException {
    final InputStream resourceAsStream = getResourceAsStream(parameterContext);
    final Parameter parameter = parameterContext.getParameter();
    if (parameter.getType() == Reader.class) {
      return new BufferedReader(new InputStreamReader(resourceAsStream, StandardCharsets.UTF_8));
    }
    if (parameter.getType() == InputStream.class) {
      return new BufferedInputStream(resourceAsStream);
    }
    throw new ParameterResolutionException("wrong parameter type " + parameter.getType());
  }

  private InputStream getResourceAsStream(ParameterContext parameterContext) {
    return parameterContext.findAnnotation(Resource.class).map(Resource::value).filter(v -> !v.isEmpty())
        .map(getClass()::getResourceAsStream).orElseThrow(() -> new ParameterResolutionException("cannot load resource"));
  }
}
