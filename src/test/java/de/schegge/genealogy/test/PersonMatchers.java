package de.schegge.genealogy.test;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import de.schegge.genealogy.model.Event;
import de.schegge.genealogy.model.Person;

public class PersonMatchers {
	public static Matcher<Person> lastname(Matcher<String> matcher) {
		return new FeatureMatcher<Person, String>(matcher, "lastname", "lastname") {
			@Override
			protected String featureValueOf(Person actual) {
				return actual.getLastname();
			}
		};
	}

	public static Matcher<Person> birth(Matcher<Event> matcher) {
		return new FeatureMatcher<Person, Event>(matcher, "birth", "birth") {
			@Override
			protected Event featureValueOf(Person actual) {
				return actual.getBirth();
			}
		};
	}

	public static Matcher<Person> death(Matcher<Event> matcher) {
		return new FeatureMatcher<Person, Event>(matcher, "birth", "birth") {
			@Override
			protected Event featureValueOf(Person actual) {
				return actual.getDeath();
			}
		};
	}
}
