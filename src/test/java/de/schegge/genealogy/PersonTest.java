package de.schegge.genealogy;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static com.spencerwi.hamcrestJDK8Time.matchers.IsBefore.before;
import static de.schegge.genealogy.test.EventMatchers.date;
import static de.schegge.genealogy.test.EventMatchers.place;
import static de.schegge.genealogy.test.PersonMatchers.birth;
import static java.time.Month.AUGUST;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNull;

import de.schegge.genealogy.test.Resource;
import de.schegge.genealogy.test.ResourceParameterResolver;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.schegge.genealogy.model.Event;
import de.schegge.genealogy.model.Person;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ResourceParameterResolver.class)
public class PersonTest {
  @Test
  public void testHamcrest(@Resource("/Muster_GEDCOM_UTF-8.ged") Reader input) throws IOException {
    GedCom gedcom = new GedComReader().read(input);
    assertThat(gedcom.findPerson("Erika /Gabler/"), isPresentAnd(
        both(birth(place(containsString("Tempelhof"))))
            .and(birth(date(before(LocalDate.of(1968, AUGUST, 31)))))));
  }

  @Test
  public void testBoth(@Resource("/Muster_GEDCOM_UTF-8.ged") Reader input) throws IOException {
    GedCom gedcom = new GedComReader().read(input);
    Person person = gedcom.findPerson("Erika /Gabler/").orElseGet(Assertions::fail);
    assertAll("person", //
        () -> {
          Event birth = person.getBirth();
          assertAll("birth", //
              () -> assertThat(birth, place(containsString("Tempelhof"))),
              () -> assertThat(birth, date(before(LocalDate.of(1968, AUGUST, 31)))));
        }, //
        () -> {
          Event death = person.getDeath();
          assertAll("death", //
              () -> assertNull(death.getDate()), () -> assertNull(death.getPlace()));
        });
  }

  @Test
  public void testErika(@Resource("/Muster_GEDCOM_UTF-8.ged") Reader input) throws IOException {
    GedCom gedcom = new GedComReader().read(input);
    Person person = gedcom.findPerson("Erika /Gabler/").orElseGet(Assertions::fail);
    assertAll("person", //
        () -> {
          Event birth = person.getBirth();
          assertAll("birth", //
              () -> assertThat(birth, place(containsString("Tempelhof"))),
              () -> assertThat(birth, date(before(LocalDate.of(1968, AUGUST, 31)))));
        }, //
        () -> {
          Event death = person.getDeath();
          assertAll("death", //
              () -> assertNull(death.getDate()), () -> assertNull(death.getPlace()));
        });
  }
}
